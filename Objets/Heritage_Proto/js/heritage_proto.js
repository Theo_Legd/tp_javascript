/*
Auteur : Legrand Theo
Date : 04/05/2021
*/
function Personne(prmNom, prmPrenom, prmAge, prmSexe) { //constructeur des personne en commun pour eleve et prof 
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;
}

Personne.prototype.decrire = function() {   //fonction decrire qui nous donnera le nom , le prenom et l'age  
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description;
}

function Professeur(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {   //constructeur professeur qui appel le constructeur commun pour les bases et construit matiere separement 
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
}

function Eleve (prmNom, prmPrenom, prmAge, prmSexe, prmClasse) {//constructeur Eleve qui appel le constructeur commun pour les bases et construit classe separement 
    Personne.call(this,prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmClasse;
}

Professeur.prototype = Object.create(Personne.prototype);   //le proto de professeur prend les valeurs de l'object creer dans personne 
Professeur.prototype.constructor = Professeur;  //reinitialisation de la proproete constructeur de professeur pour le recuperer 

Eleve.prototype = Object.create(Personne.prototype); //le proto de Eleve prend les valeurs de l'object creer dans personne 
Eleve.prototype.constructor = Eleve; //reinitialisation de la proproete constructeur de professeur pour le recuperer 





Professeur.prototype.decrire_plus = function() {    //fonction decrire plus pour le professeur qui prend en compte le prototype du professeur 
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'Mr';
    } else {
        prefixe = 'Mme';
    }
    description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
    return description;
}

Eleve.prototype.decrire_plus = function() {     //fonction decrire plus pour le professeur qui prend en compte le prototype de eleve 
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'un';
    } else {
        prefixe = 'une';
    }
    description = this.prenom + " " + this.nom + " est " +prefixe +" élève de " +this.classe ;
    return description;
}




let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');    //creation objet dans professeur 
console.log(objProfesseur1.decrire());  //appel decrire lie au prototype de Personne pour l'object professeur1 cree 
console.log(objProfesseur1.decrire_plus());//appel fonction decrire_plus lie au prototype de Professeur pour l'object professeur1 cree

let objEleve1 = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');    //creation objet dans eleve 
console.log(objEleve1.decrire());//appel decrire lie au prototype de Personne pour l'object eleve1 cree 
console.log(objEleve1.decrire_plus());//appel fonction decrire_plus lie au prototype de Eleve pour l'object eleve1 cree




