/*
Auteur : Legrand Theo
Date : 26/04/2021
*/
//valeurs de basse correspondante en euros est en dollars
let euros = 1;
let dollars = 1.65;

while (euros <= 16384) {
  //faire tant que la valeurs est en desous de 16384
  console.log(euros + " euros = " + dollars.toFixed(2) + " dollars"); //affichage de la correspondance des deux valeurs dans la console en limitant dollars à 2 chiffre apres la virgule

  // on multi^plie les deux vvaleurs les deux variables de facon a avoir des valeurs correspondante
  euros = euros * 2;
  dollars = dollars * 2;
}
