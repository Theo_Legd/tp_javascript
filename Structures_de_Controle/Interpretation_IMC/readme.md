## Test du projet Interpretation IMC
Test affichage console:

-Test Principale
![img_test1](images/test_princ.png)

-"Dénutrition"
![img_test1](images/denutrition.png)

-"Maigreur"
![img_test1](images/maigreur.png)

-"Corpulence normale"
![img_test1](images/normale.png)

-"Surpoids"
![img_test1](images/surpoids.png)

-"Obesité modérée"
![img_test1](images/ob_moderee.png)

-"Obesité sévère"
![img_test1](images/ob_severe.png)

-"Obésité morbide"
![img_test1](images/ob_morbide.png)

