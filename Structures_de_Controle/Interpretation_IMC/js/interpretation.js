/*
Auteur : Legrand Theo
Date : 26/04/2021
*/
//init des variables
let taille = 160; // choix de la taille 
let poids = 100;  //choix du poids
let affichage = "Interpretation de l'IMC : ";

console.log("Calcul de l'IMC :");
let IMC = poids / ((taille / 100) * (taille / 100));  //calcul de L'IMC
//let IMC = 45; // ligne permettant de faire varier l'IMC pour tester chaque resultats  

if (IMC < 16.5) {                   //si IMC est inferieur à 16.5
  affichage = affichage + "Dénutrition";
} else if (IMC < 18.5) {//si IMC est inferieur à 18.5
  affichage = affichage + "Maigreur";
} else if (IMC < 25) {//si IMC est inferieur à 25
  affichage = affichage + "Corpulence normale";
} else if (IMC < 30) {//si IMC est inferieur à 30
  affichage = affichage + "Surpoids";
} else if (IMC < 35) {//si IMC est inferieur à 35
  affichage = affichage + "Obesité modérée";
} else if (IMC < 40) {//si IMC est inferieur à 40
  affichage = affichage + "Obesité sévère";
} else if (IMC > 40) {//si IMC est superieur à 40
  affichage = affichage + "Obésité morbide";
}

console.log("taille:" + taille + "cm");// affichage de la taille
console.log("poids:" + poids + "kg");// affichage du poids
console.log("IMC " + IMC.toFixed(1)); // affichage de l'IMC

console.log(affichage);// affichage de l'interpretation de l'IMC
