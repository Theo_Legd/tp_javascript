/*
Auteur : Legrand Theo
Date : 26/04/2021
*/
//init des variables

let res = 7; //resultat du calcul
let affichage = ""; // init chaine de caract

for (let i = 1; i < 21; i++) {
  //pour i allant de 1 a 21 avec un pas de 1
  res = 7 * i; //multplication du nombre par 7

  if (res % 3 == 0) {
    // si le nombre est un multiple de 3 faire
    affichage = affichage + " " + res + "*";
  } else {
    // sinon faire
    affichage = affichage + " " + res; // ajout du nombre obtenue dans la chaine
  }
}
console.log(affichage); // affichages de chaine de caractere obtenue dans la console
