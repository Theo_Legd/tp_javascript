/*
Auteur : Legrand Theo
Date : 26/04/2021
*/
//init des variables
let nb = 14; // Choix du nombre de base
let valmax = 0; // altitude maximale atteinte
let cpt = 0; //compteur de tours
let affichage = "14"; // init de la chaine de caract avec 14 en premier nombre

console.log("Suite de Syracuse pour " + nb + ":");
//Afffichage du nombre choisi pour la suite dans la console
if (nb < 1) {
  //si nb est inferieur a 1
  console.log("Erreur nb est inferieur à 1"); //afficher une erreur dans la console
} else {
  // sinon
  while (nb != 1) {
    // tant que nb est different de 1
    if (nb > valmax) {
      //si nb est superieur a la valeur max atteinte
      valmax = nb; // valmax est egale a nb
    }
    if (nb % 2 == 0) {
      // si nb est pair
      nb = nb / 2; // nb est divise par 2
    } else {
      //sinon
      nb = nb * 3 + 1; // on multiplie nb par 3 est on ajoute 1
    }

    affichage = affichage + " " + nb; // on ajoute le resultat obtenue a la chaine de caract avec un espace
    cpt++; //incrementation du compteur
  }
  console.log(affichage); // afficher la chaine de caractere
  console.log("Le temps de vol est de : " + cpt); // afficher le nombre de tours
  console.log("La valeur maximale atteinte est : " + valmax); // afficher la valeur max atteinte
}
