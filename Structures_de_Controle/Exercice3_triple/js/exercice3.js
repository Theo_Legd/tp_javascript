/*
Auteur : Legrand Theo
Date : 26/04/2021
*/
let nombre = 2;     //init du nombre de base 2 
let affichage = "";     //init de la chaine de caractere aui contiendra les resultats
console.log("Valeur de départ : " + nombre);
for (let i = 1; i < 12; i++) {    //pour i allant de 1 à 12 avec un pas de 1
  affichage = affichage + nombre + " ";         // affichage est egale a nombre plus un espace 
  nombre = nombre * 3;              //nombre est triple 
}
console.log("Valeurs de la suite : " + affichage);      //affichage de la chaine de caractere obtenue 
