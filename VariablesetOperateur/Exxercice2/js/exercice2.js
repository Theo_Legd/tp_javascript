let tempC = 22.6;

console.log("Conversion Celsius (°C) / Fahrenheit (°F) ");
let tempF = tempC * 1.8 + 32;

console.log(
  "une température de " +
    tempC +
    "°C correspond à une temperature de " +
    tempF.toFixed(1) +
    "°F"
);
