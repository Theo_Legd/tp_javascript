/*
Auteur : Legrand Theo
Date : 05/05/2021
*/
//creation d'un tableau par article
let article1 = new Array("Jus d'orange 1L", 1.35, 2);
let article2 = new Array("Yaourt nature 4X", 1.6, 1);
let article3 = new Array("Pain de mie 500g", 0.9, 1);
let article4 = new Array("Barquette Jambon blanc 4xt", 2.75, 1);
let article5 = new Array("Salade Laitue", 0.8, 1);
let article6 = new Array("Spaghettis 500g", 0.95, 2);
//creation tableau qui regroupe tout les articles
let listeArticle = new Array(
  article1,
  article2,
  article3,
  article4,
  article5,
  article6
);

let nbArticles = 0; //variable comptant le nnombre d'article
let prixTotale = 0; //variable qui stockera le prix total

for (let article of listeArticle) {
  let calcul = 0; //variable pour le calcul du prix d'un article
  console.log(article[0]); //affichage du nom de l'aticle
  calcul = article[1] * article[2]; //calcule du prix
  console.log(
    article[2] + " X " + article[1] + "EUR    " + calcul.toFixed(2) + " EUR"
  ); //affichage du calcul du prix et du resultat
  prixTotale = prixTotale + calcul; //ajout du prix de l'article au prix totale
  nbArticles = nbArticles + article[2]; //comptage du n'ombre d'article
}

console.log("Nombre d'articles achetés: " + nbArticles); // affichage du nombre d'article achete
console.log("MONTANT: " + prixTotale.toFixed(2) + " EUR"); // affichage du prix totale
