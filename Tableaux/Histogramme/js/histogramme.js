/*
Auteur : Legrand Theo
Date : 18/05/2021
*/
let TabNotes = [
  19,
  12,
  14,
  5,
  12,
  8,
  3,
  12,
  8,
  4,
  19,
  16,
  14,
  8,
  12,
  14,
  12,
  11,
  12,
  4,
  11,
  13,
];

console.log(TabNotes);

calculerRepartition = function (prmTabNotes) {
  let repartition = [
    [0, 0],
    [1, 0],
    [2, 0],
    [3, 0],
    [4, 0],
    [5, 0],
    [6, 0],
    [7, 0],
    [8, 0],
    [9, 0],
    [10, 0],
    [11, 0],
    [12, 0],
    [13, 0],
    [14, 0],
    [15, 0],
    [16, 0],
    [17, 0],
    [18, 0],
    [19, 0],
    [20, 0],
  ];
  for (let i = 0; i < TabNotes.length; i++) {
    repartition[TabNotes[i]][1]++;
  }
  console.log(repartition);
  return repartition;
};

let repart_finale = calculerRepartition(TabNotes);

tracerHisto = function (prmRepartition) {
    
  for (let notes of prmRepartition) {
   console.log(notes[0] + "\t" + ("*").repeat(notes[1]))
  }
};

tracerHisto(repart_finale);
