/*
Auteur : Legrand Theo
Date : 05/05/2021
*/

let m = "masculin";
let f = "feminin";

function Patient(prmNom, prmPrenom, prmAge, prmSexe, prmTaille, prmPoids) {
  //creation de la fonction decrire
  this.nom = prmNom;
  this.prenom = prmPrenom;
  this.sexe = prmSexe;
  this.age = prmAge;
  this.taille = prmTaille;
  this.poids = prmPoids;
  this.IMC = this.poids / ((this.taille / 100) * (this.taille / 100));
}

//cration des objects patient
let objPatient0 = new Patient("Dupond", "Jean", 30, "masculin", 180, 85);
let objPatient1 = new Patient("Martin", "Eric", 42, "masculin", 165, 90);
let objPatient2 = new Patient("Moulin", "Isabelle", 46, "féminin", 158, 74);
let objPatient3 = new Patient("Verwaerde", "Paul", 55, "masculin", 177, 66);
let objPatient4 = new Patient("Durand", "Dominique", 60, "féminin", 163, 54);
let objPatient5 = new Patient("Lejeune", "Bernard", 63, "masculin", 158, 78);
let objPatient6 = new Patient("Chevalier", "Louise", 35, "féminin", 170, 82);
//creation du tableau contenant les patients
let tabPatients = new Array(
  objPatient0,
  objPatient1,
  objPatient2,
  objPatient3,
  objPatient4,
  objPatient5,
  objPatient6
);

afficher_ListePatients = function (prmTabPatients) {
  //creation de la fonction afficher patients
  let affichage = ""; //creation chaine de caractere de base
  for (let i = 0; i < tabPatients.length; i++) {
    //pour i allant de 0 a la taille du tableaux Faire
    affichage = prmTabPatients[i].nom + "  " + prmTabPatients[i].prenom; //affichage prend la valaeur de du parametre nom + prenom du tableau tabpatients a l'index i
    console.log(affichage); //afficher nom + prenom
  }
};

afficher_ListePatients_Par_Sexe = function (prmTabPatients, prmSexe) {
  sexe = prmSexe;
  if (sexe == "masculin") {
    //si le sexe dans le parametre est masculin
    console.log("\nListe des patients de sexe masculin :"); //afficher Liste des patients de sexe masculin :  "
    let affichage = "";
    for (let i = 0; i < tabPatients.length; i++) {
      //pour i allant de 0 a la taille du tableaux Faire
      if (prmTabPatients[i].sexe == "masculin") {
        //si le sexe du patient est masculin
        affichage = prmTabPatients[i].nom + "  " + prmTabPatients[i].prenom; //affichage prend la valaeur de du parametre nom + prenom du tableau tabpatients a l'index i
        console.log(affichage); //afficher nom + prenom
      }
    }
  } else if (sexe == "feminin") {
    //si le sexe dans le parametre est feminin
    console.log("\nListe des patients de sexe féminin :"); //afficher Liste des patients de sexe féminin: "
    for (let i = 0; i < tabPatients.length; i++) {
      //pour i allant de 0 a la taille du tableaux Faire
      if (prmTabPatients[i].sexe == "féminin") {
        //si le sexe du patient est féminin
        affichage = prmTabPatients[i].nom + "  " + prmTabPatients[i].prenom; //affichage prend la valaeur de du parametre nom + prenom du tableau tabpatients a l'index i
        console.log(affichage); //afficher nom + prenom
      }
    }
  }
};

afficher_ListePatients_Par_Corpulence = function (
  prmTabPatients,
  prmCorpulence
) {
  let corpulence = prmCorpulence;
  let retour = "";
  console.log("Liste des patients en états de " + corpulence);
  let affichage = ""; //creation de la chaine de caractere affichage

  for (let i = 0; i < tabPatients.length; i++) {
    let imcSexe = prmTabPatients[i].IMC;

    if (prmTabPatients[i].sexe == "masculin") {
      imcSexe = prmTabPatients[i].IMC - 2;
    }

    if (imcSexe < 16.5) {
      //si IMC est inferieur à 16.5
      affichage = "Dénutrition";
    } else if (imcSexe < 18.5) {
      //si IMC est inferieur à 18.5
      affichage = "Maigreur";
    } else if (imcSexe < 25) {
      //si IMC est inferieur à 25
      affichage = "Corpulence normale";
    } else if (imcSexe < 30) {
      //si IMC est inferieur à 30
      affichage = "Surpoids";
    } else if (imcSexe < 35) {
      //si IMC est inferieur à 35
      affichage = "Obesité modérée";
    } else if (imcSexe < 40) {
      //si IMC est inferieur à 40
      affichage = "Obesité sévère";
    } else if (imcSexe > 40) {
      //si IMC est superieur à 40
      affichage = "Obésité morbide";
    }
    if (affichage == prmCorpulence) {
      //affichage du patient
      retour =
        retour +
        prmTabPatients[i].prenom +
        " " +
        prmTabPatients[i].nom +
        " avec un IMC = " +
        prmTabPatients[i].IMC.toFixed(2) +
        "\n";
    }
  }
  console.log(retour);
};

afficher_DescriptionPatient = function (prmTabPatients, prmNom) {
  console.log("Description du patient " + prmNom);
  let corpu ="" ;
  let affichage = "Ce patient n'existe pas" ;
  for (let i = 0; i < tabPatients.length; i++) {
    if (prmTabPatients[i].nom == prmNom) {
      let m = Math.floor(prmTabPatients[i].taille / 100);
      let cm = prmTabPatients[i].taille % 100 ;
      let imcSexe = prmTabPatients[i].IMC;

      if (prmTabPatients[i].sexe == "masculin") {
        imcSexe = prmTabPatients[i].IMC - 2;
      }

      if (imcSexe < 16.5) {
        //si IMC est inferieur à 16.5
        corpu = "Dénutrition";
      } else if (imcSexe < 18.5) {
        //si IMC est inferieur à 18.5
        corpu = "Maigreur";
      } else if (imcSexe < 25) {
        //si IMC est inferieur à 25
        corpu = "Corpulence normale";
      } else if (imcSexe < 30) {
        //si IMC est inferieur à 30
        corpu = "Surpoids";
      } else if (imcSexe < 35) {
        //si IMC est inferieur à 35
        corpu = "Obesité modérée";
      } else if (imcSexe < 40) {
        //si IMC est inferieur à 40
        corpu = "Obesité sévère";
      } else if (imcSexe > 40) {
        //si IMC est superieur à 40
        corpu = "Obésité morbide";
      }
      if (prmTabPatients[i].sexe == 'féminin') {
        affichage = "La patiente " + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + " est agée de " + prmTabPatients[i].age + " ans. Elle mesure " + m + "m" +cm +  " et pèse " + prmTabPatients[i].poids + " kg\n L'IMC de ce patient est de "+ prmTabPatients[i].IMC.toFixed(2)+ "\n";
        affichage = affichage + "\nElle est en situation de : " + interpretation + "\n";

    } else {
      affichage = "Le patient " + prmTabPatients[i].prenom + " " + prmTabPatients[i].nom + " est agé de " + prmTabPatients[i].age + " ans. Il mesure " + m + "m" +cm + " et pèse " + prmTabPatients[i].poids + " kg\nL'IMC de ce patient est de "+ prmTabPatients[i].IMC.toFixed(2)+ "\n";
      affichage = affichage + "\nIl est en situation de : " + corpu + "\n ";
    }

    }
  }
  console.log(affichage);
};
console.log("Liste des patients : "); //afficher Liste des patients : "
afficher_ListePatients(tabPatients); //appel de la fonction afficher_ListePatients
afficher_ListePatients_Par_Sexe(tabPatients, m); //appel de la fonction afficher_ListePatients_Par_Sexe pour les hommes
afficher_ListePatients_Par_Sexe(tabPatients, f); //appel de la fonction afficher_ListePatients_Par_Sexe pour les femmes
afficher_ListePatients_Par_Corpulence(tabPatients, "Surpoids"); // Appel de la fonction et affichage.

afficher_DescriptionPatient(tabPatients, 'Dupond'); 
afficher_DescriptionPatient(tabPatients, 'Brassart'); 
