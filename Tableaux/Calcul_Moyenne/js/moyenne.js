/*
Auteur : Legrand Theo
Date : 04/05/2021
*/
let listeValeurs = [10,25,41,5,9,11];   // creation tableau 
let resultat_s = 0 ;    //variable qui contiendra le resultats de la somme des nombre 
let resultat_m = 0 ;    //variable qui contiendra les resultat de la moyenne 
let cpt =0 ;   //variable compteur de boucle a chaque ajou d'un nombre a resultats_s 

for(let nombre of listeValeurs){    //affichage des differents nombre contenue dans le tableau 
    console.log ( nombre);
} 

for(let calcul of listeValeurs){    //somme de tout les nombre dans le tableau 
    resultat_s = resultat_s + calcul ;  //calcul de la somme 
    cpt ++ ;    //incrementation de cpt 
}

resultat_m = resultat_s / cpt ; //calcul de la moyenne 


console.log ("Somme des valeurs : " +resultat_s);   //affichage somme 

console.log ("Moyenne des valeurs : " +resultat_m.toFixed(2));  //affichage moyenne 