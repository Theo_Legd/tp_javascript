/*
Auteur : Legrand Theo
Date : 27/04/2021
*/
//init des variables
console.log("Calcul de l'IMC :");

function decrire_corpulence(prmTaille, prmPoids) {//creation fonction generale 
  function calculerIMC(prmTaille, prmPoids) {   //creation fonction calculer IMC 
    let IMCVALEUR;    
    IMCVALEUR = prmPoids / ((prmTaille / 100) * (prmTaille / 100)); //calcul de L'IMC

    return IMCVALEUR;
  }
  //let IMC = 45; // ligne permettant de faire varier l'IMC pour tester chaque resultats
  let prmIMC = calculerIMC(prmTaille, prmPoids);

  function interpreterIMC(prmIMC) {     //
    let affichage = "Interpretation de l'IMC : "; //creation de la chaine de caractere affichage
    if (prmIMC < 16.5) {
      //si IMC est inferieur à 16.5
      affichage = affichage + "Dénutrition";
    } else if (prmIMC < 18.5) {
      //si IMC est inferieur à 18.5
      affichage = affichage + "Maigreur";
    } else if (prmIMC < 25) {
      //si IMC est inferieur à 25
      affichage = affichage + "Corpulence normale";
    } else if (prmIMC < 30) {
      //si IMC est inferieur à 30
      affichage = affichage + "Surpoids";
    } else if (prmIMC < 35) {
      //si IMC est inferieur à 35
      affichage = affichage + "Obesité modérée";
    } else if (prmIMC < 40) {
      //si IMC est inferieur à 40
      affichage = affichage + "Obesité sévère";
    } else if (prmIMC > 40) {
      //si IMC est superieur à 40
      affichage = affichage + "Obésité morbide";
    }
    return affichage;
  }
  let interIMC = interpreterIMC(prmIMC);
  let info =
    "la valeur de l'IMC de cet individu est " +
    prmIMC.toFixed(1) +
    " et l'interprétation de l'imc est : " +
    interIMC;
  return info;
}
let result = decrire_corpulence(160, 100);
console.log(result); // affichage de l'interpretation de l'IMC
