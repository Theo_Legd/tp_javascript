/*
Auteur : Legrand Theo
Date : 26/04/2021
*/
//init des variables
let interIMC ="";
let IMC = 0;
let taille = 160; // choix de la taille
console.log("taille:" + taille + "cm"); // affichage de la taille
let poids = 100; //choix du poids
console.log("poids:" + poids + "kg"); // affichage du poids

console.log("Calcul de l'IMC :");
function calculerIMC(prmTaille, prmPoids){
    let IMCVALEUR ;
    IMCVALEUR = prmPoids / ((prmTaille / 100) * (prmTaille / 100)); //calcul de L'IMC
    return IMCVALEUR ;
}
//let IMC = 45; // ligne permettant de faire varier l'IMC pour tester chaque resultats
IMC = calculerIMC (taille,poids);
console.log("IMC " + IMC.toFixed(1)); // affichage de l'IMC

let affichage = "Interpretation de l'IMC : "; //creation de la chaine de caractere affichage 

function interpretationIMC(prmIMC){
    let affichage = "Interpretation de l'IMC : "; //creation de la chaine de caractere affichage 
    if (prmIMC < 16.5) {
  //si IMC est inferieur à 16.5
    affichage = affichage + "Dénutrition";

}    else if (prmIMC < 18.5) {
  //si IMC est inferieur à 18.5
    affichage = affichage + "Maigreur";

}   else if (prmIMC < 25) {
  //si IMC est inferieur à 25
    affichage = affichage + "Corpulence normale";

}   else if (prmIMC < 30) {
  //si IMC est inferieur à 30
    affichage = affichage + "Surpoids";

}   else if (prmIMC < 35) {
  //si IMC est inferieur à 35
    affichage = affichage + "Obesité modérée";

}   else if (prmIMC < 40) {
  //si IMC est inferieur à 40
    affichage = affichage + "Obesité sévère";

}   else if (prmIMC > 40) {
  //si IMC est superieur à 40
    affichage = affichage + "Obésité morbide";

}
return affichage ;
}
interIMC = interpretationIMC(IMC)
console.log(interIMC); // affichage de l'interpretation de l'IMC
