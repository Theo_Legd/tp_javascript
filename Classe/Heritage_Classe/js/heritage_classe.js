/*
Auteur : Legrand Theo
Date : 19/05/2021
*/
class Personne{
constructor(prmNom, prmPrenom, prmAge, prmSexe) { //constructeur des personne en commun pour eleve et prof 
    this.nom = prmNom;
    this.prenom = prmPrenom;
    this.age = prmAge;
    this.sexe = prmSexe;

}
    decrire = function() {   //fonction decrire qui nous donnera le nom , le prenom et l'age  
    let description;
    description = "Cette personne s'appelle " + this.prenom + " " + this.nom + " elle est agée de " + this.age + " ans" ;
    return description;
}
}

class Professeur extends Personne{//on creer la classe professeur qui herite de la classe personne 
constructor(prmNom, prmPrenom, prmAge, prmSexe, prmMatiere) {   //constructeur professeur qui appel le constructeur commun pour les bases et construit matiere separement 
    super(prmNom, prmPrenom, prmAge, prmSexe);
    this.matiere = prmMatiere;
    }

    decrire_plus () {    //fonction qui fonctionne seulement pour professeur 
        let description;
        let prefixe;
        if(this.sexe == 'M') {
            prefixe = 'Mr';
        } else {
            prefixe = 'Mme';
        }
        description = prefixe + " " + this.prenom + " " + this.nom + " est professeur de " + this.matiere;
        return description;
    }


}




class Eleve extends Personne { //on creer la classe eleve qui herite de la classe personne 
constructor (prmNom, prmPrenom, prmAge, prmSexe, prmClasse) {//constructeur Eleve qui appel le constructeur commun pour les bases et construit classe separement 
    super(prmNom, prmPrenom, prmAge, prmSexe);
    this.classe = prmClasse;
}
decrire_plus () {      //fonction qui fonctionne seulement pour eleve 
    let description;
    let prefixe;
    if(this.sexe == 'M') {
        prefixe = 'un';
    } else {
        prefixe = 'une';
    }
    description = this.prenom + " " + this.nom + " est " +prefixe +" élève de " +this.classe ;
    return description;
}




}

let objProfesseur1 = new Professeur('Dupond', 'Jean', 30, 'M', 'Mathématiques');    //creation objet dans professeur 
console.log(objProfesseur1.decrire());  //appel decrire lie au prototype de Personne pour l'object professeur1 cree 
console.log(objProfesseur1.decrire_plus());//appel fonction decrire_plus lie au prototype de Professeur pour l'object professeur1 cree

let objEleve1 = new Eleve('Dutillieul', 'Dorian', 18, 'M', 'SNIR1');    //creation objet dans eleve 
console.log(objEleve1.decrire());//appel decrire lie au prototype de Personne pour l'object eleve1 cree 
console.log(objEleve1.decrire_plus());//appel fonction decrire_plus lie au prototype de Eleve pour l'object eleve1 cree


