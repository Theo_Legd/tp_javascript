/*
Auteur : Legrand Theo
Date : 19/05/2021
*/

class Personnage {
  constructor(prmNom, prmNiveau) {
  
    this.nom = prmNom;
    this.niveau = prmNiveau;
  }
  saluer() {
    
    let presentation;
    presentation = this.nom + " vous salue !!";
    return presentation;
  }
}

class Guerrier extends Personnage {
  constructor(prmNom, prmNiveau, prmArme) {
    super(prmNom, prmNiveau);
    this.arme = prmArme;
  }

  combattre() {
    let description;
    description = this.nom + " est un guerrier qui se bat avec " + this.arme;
    return description;
  }
}

class Magicien extends Personnage {
  constructor(prmNom, prmNiveau, prmPouvoir) {
    super(prmNom, prmNiveau);
    this.pouvoir = prmPouvoir;
  }
  posseder() {
    let description;
    description =
      this.nom + " est un magicien qui possède le pouvoir de " + this.pouvoir;
    return description;
  }
}

let objPersonnage1 = new Guerrier("Arthur", 3, "une épée");

console.log(objPersonnage1.saluer());

console.log(objPersonnage1.combattre());

let objPersonnage2 = new Magicien("Merlin", 2, "prédire les batailles");

console.log(objPersonnage2.saluer());

console.log(objPersonnage2.posseder());
