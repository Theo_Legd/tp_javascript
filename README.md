# TP_JavaScript

Mes TP en Javascript

> - Auteur : Theo LEGRAND
> - Date de publication : 06/04/2021

## Introduction
- [HelloWorld](Introduction/HelloWorld_JS/js/helloworld.js)



## Variables et Opérateurs
- [Exercice 1](VariablesetOperateur/Exercice1/js/exercie1.js)
- [Exercice 2](VariablesetOperateur/Exxercice2/js/exercice2.js)

## Structure de contôle
- [Exercice 1 (Calcul de factorielle)](Structures_de_Controle/Exercice1/js/exercice1.js)
- [Exercice 2 (Conversion Euros/Dollars)](Structures_de_Controle/Exercice2_conversion/js/exercice2.js)
- [Exercice 3 (Nombres triples)](Structures_de_Controle/Exercice3_triple/js/exercice3.js)
- [Exercice 4 (Suite de Fibonacci)](Structures_de_Controle/Exercice4_fibo/js/exercice4.js)
- [Exercice 5 (Table de multiplication)](Structures_de_Controle/Exercice5_multiplication/js/exercice5.jss)
- [Activité (Interprétation de l'IMC)](Structures_de_Controle/Interpretation_IMC/js/interpretation.js)
- [Activité (Conjecture de Syracuse)](Structures_de_Controle/Suite_Syracuse/js/syracuse.js)

## Les Fonctions
- [IMC](Fonctions/IMC/js/imc.js)
- [Exercice 1 (Temps_Parcours)](Fonctions/Exercice1_temps_de_parcours/js/tempparcours.js)
- [Exercice 2 (Multiple_3)](Fonctions/EXO2_mult_3/js/mult3.js)
- [IMC2](Fonctions/IMC2/js/imcv2.js)

## Les Objets
- [IMC1 obj](Objets/imc_obj1/js/imc_obj.js)
- [IMC2 obj](Objets/imc_obj2/js/imc_obj2.js)
- [IMC obj_opti](Objets/imc_obj_opti/js/imc_obj_opti.js)
- [Heritage Proto](Objets/Heritage_Proto/js/heritage_proto.js)

## Les Tableaux
- [Calcul Moyenne](Tableaux/Calcul_Moyenne/js/moyenne.js)
- [Liste Articles](Tableaux/Liste_Articles/js/liste_articles.js)
- [Tableau_Patient_IMC](Tableaux/Tableau_Patient_IMC/js/tab_Patient_IMC.js)
- [Histogramme](Tableaux/Histogramme/js/histogramme.js)

## Les Classes
- [IMC_CLasse](Classe/activite_imc/js/imc.js)
- [Classe_Heritage](Classe/Heritage_Classe/js/heritage_classe.js)
- [Classe_Jeu](Classe/exo_jeu/js/jeu_classe.js)